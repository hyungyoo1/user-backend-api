export const USER_CONFLICT_RESPONSE = "Bad request. The email already exists";
export const USER_CREATED_RESPONSE =
  "User information was created successfully";
export const USER_OK_RESPONSE = "User information was updated successfully";
export const USER_NOT_FOUND =
  "The user corresponding to the given ID does not exis";
export const USER_UNPROCESSABLE_ENTITY = "Error hashing password";
